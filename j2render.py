#!/bin/env python3
#
# Script which uses Jinja2 to render a template using one or more data sources.
#
# See ./j2render.py --help for help
#
import getopt
import os
import sys
import inspect
import importlib.util as iu
import csv
import io

if sys.version_info.major != 3 or sys.version_info.minor < 6:
    print("This application requires at least python 3.6", file=sys.stderr)
    sys.exit(1)

try:
    import jinja2 as j2
except ImportError:
    print("This application requires the jinja2 template engine, install using pip3 install jinja2", file=sys.stderr)
    sys.exit(1)


# Attempt to import JSON5, otherwise json
try:
    import json5 as json
    JSON5 = True
except ModuleNotFoundError:
    import json
    JSON5 = False

try:
    import yaml
    YAML = True
except ImportError:
    YAML = False

    class Yaml:     # Dummy class
        def load(*_):
            raise RuntimeError("YAML support not installed")
    yaml = Yaml()

STRIP_EXT = (".j2", ".jinja", ".jinja2", ".tmpl")
PCOUNT = 0


def load_proc(pyfile: str):
    global PCOUNT

    if not os.path.exists(pyfile):
        print("Python module %s does not exist" % pyfile, file=sys.stderr)
        sys.exit(1)

    proc_spec = iu.spec_from_file_location("proc%d" % PCOUNT, pyfile)

    if proc_spec is None:
        print("Failed to load file %s" % pyfile, file=sys.stderr)
        sys.exit(1)

    PCOUNT += 1
    proc_mod = iu.module_from_spec(proc_spec)
    proc_spec.loader.exec_module(proc_mod)
    return proc_mod


class J2Render:

    def __init__(self):
        self._context = {}
        self._processors = []

    def add_context(self, context, namespace=None):
        if namespace:
            self._context[namespace] = context
        else:
            self._context.update(context)

    def add_processor(self, procmod):
        if not hasattr(procmod, 'proc'):
            raise RuntimeError(f"Processor {procmod} must have a function named 'proc'")
        ffunc = procmod.proc
        if not callable(ffunc):
            raise RuntimeError(f"Object 'proc' inside {procmod} is not callable")
        p = len(inspect.signature(ffunc).parameters)
        if p < 1 or p > 2:
            raise RuntimeError(f"Callback 'proc' inside {procmod} must have one or two parameters")
        self._processors.append((ffunc, p == 2))

    def render(self, template_file, output_stream):
        template_path = os.path.dirname(template_file)
        template_filename = os.path.basename(template_file)
        env = j2.Environment(
            loader=j2.FileSystemLoader(template_path)
        )
        ctx = self._context.copy()
        for proc, has_env_param in self._processors:
            if has_env_param:
                ctx2 = proc(ctx, env)
            else:
                ctx2 = proc(ctx)
            if ctx2 is not None:
                ctx = ctx2
        template = env.get_template(template_filename)

        output_stream.write(template.render(ctx))


class J2RenderCLI:

    def __init__(self):
        self.__jr = J2Render()
        self.__options = []
        self.__shorts = ""
        self.__out = None
        self.__dir = None
        self.__delim = ","
        self.__dialect = 'excel'
        self.__long = []
        self.__namespace = None
        self.__opt2info = {}

        for field in dir(self):
            if not field.startswith("opt_"):
                continue
            arg_names = field[4:].split("__")
            func = getattr(self, field)
            params = inspect.signature(func).parameters
            if len(params) > 1:
                raise RuntimeError("Option can not have more than one Parameter")
            has_param = len(params) > 0
            if has_param:
                param_name = next(iter(params.keys())).upper()
            else:
                param_name = None

            desc = func.__doc__
            short = None
            # Has short:
            if len(arg_names) == 2:
                self.__shorts += f"{arg_names[0]}:" if has_param else arg_names[0]
                short = f"-{arg_names[0]}"
                arg_names = arg_names[1:]

            arg_names[0] = arg_names[0].replace("_", "-")
            self.__long.append(f"{arg_names[0]}=" if has_param else f"{arg_names[0]}")
            long = f"--{arg_names[0]}"

            opt_info = (long, short, has_param, func, desc, param_name)
            self.__options.append(opt_info)
            self.__opt2info[long] = opt_info
            if short:
                self.__opt2info[short] = opt_info

    def opt_h__help(self):
        """Shows this help"""
        self.show_usage()
        sys.exit(0)

    def opt_n__namespace(self, ns):
        """Namespace for the next data"""
        self.__namespace = ns

    def opt_p__proc(self, pyfile):
        """Add a context processor (python module)"""
        procmod = load_proc(pyfile)
        self.__jr.add_processor(procmod)

    def opt_j__json(self, file):
        """Input json(5*) data file"""
        with open(file, "r") as stream:
            self._add_context(json.load(stream))

    def opt_y__yaml(self, file):
        """Input yaml(**) data file"""
        with open(file, "r") as stream:
            self._add_context(yaml.load(stream))

    def opt_t__table(self, csvfile):
        """Input table data (.csv)"""
        if self.__namespace is None:
            base_name = os.path.basename(csvfile)
            self.__namespace = base_name = os.path.splitext(base_name)[0]

        with open(csvfile, "r") as stream:
            dataset = [row for row in csv.DictReader(stream, dialect=self.__dialect, delimiter=self.__delim)]

        self._add_context(dataset)

    def opt_table_delim(self, char):
        """Specify table delimiter, specify before next table data***"""
        self.__delim = char

    def opt_table_dialect(self, dialect):
        """Specify table dialect, specify before next table data***"""
        self.__dialect = dialect

    def opt_c__context(self, jtext):
        """Input JSON(5*)-formatted data directly from the command-line"""
        self._add_context(json.loads(jtext))

    def opt_o__out(self, outfile):
        """File to write the output to"""
        if self.__dir:
            raise RuntimeError("Can't use --out/-o together with --dir/-d")
        self.__out = outfile

    def opt_d__dir(self, outdir):
        """Directory to write the output of templates to"""
        if self.__out:
            raise RuntimeError("Can't use --out/-o together with --dir/-d")
        self.__dir = outdir

    def _render(self, template_file):
        if self.__out:
            with open(self.__out, "w") as stream:
                self.__jr.render(template_file, stream)
        elif self.__dir:
            output_file = os.path.basename(template_file)
            for ext in STRIP_EXT:
                if output_file.endswith(ext):
                    output_file = output_file[:-len(ext)]
                    break
            output_file = os.path.join(self.__dir, output_file)
            if os.path.abspath(output_file) == os.path.abspath(template_file):
                raise RuntimeError(f"Can't overwrite template file: {template_file} !")

            with io.open(output_file, "w", encoding="utf-8") as stream:
                self.__jr.render(template_file, stream)

    def _add_context(self, context):
        self.__jr.add_context(context, namespace=self.__namespace)
        self.__namespace = None

    def show_usage(self):
        print("""\
j2render.py [options] <template-file> [<template-file>]
""")
        for long, short, has_param, func, desc, param_name in self.__options:
            if has_param:
                args = [f"{long}={param_name}"]
                if short:
                    args.append(f"{short} {param_name}")
            else:
                args = [long]
                if short:
                    args.append(f"{short}")
            args = ",".join(args)

            print(f"{args:30s} {desc}")
        print("""\

  * json5 package must be installed (`pip3 install json5`)
 ** yaml package must be installed (`pip3 install pyyaml`)
*** Unlike namespaces the csv properties are not reset after data input. Thus
    when using these flags to read multiple tables with different delimiters
    or dialects you will need to specify them explicitly before each read.

When using the --dir option, the output filename will be the template name. If
the template ends with .jinja2, .j2, .jinja or .tmpl the extension will be
stripped from the output filename. E.g. 'helloworld.html.j2' will be rendered
into a file named 'helloworld.html'.

Tables are expected in .csv format and the first row must be a header. They
are included row first, as a list of dicts, where each contains the column
header as key and the value for that column at that row. When including a
table the filename, stripped of its last extension, will be used as a
namespace, unless another namespace has been specified first (using --ns). By
default a comma is expected as separator. You can change the separator using
--table-sep option, before issuing the --table option. 
""")
        print(f"Support: Yaml={YAML}, JSON5={JSON5}")

    def process_arg(self, *argv):
        try:
            opts, args = getopt.getopt(argv, self.__shorts, self.__long)
        except getopt.GetoptError as err:
            print(err)  # will print something like "option -a not recognized"
            self.show_usage()
            sys.exit(1)

        for opt, val in opts:

            if opt not in self.__opt2info:
                print(f"Unknown option {opt}", file=sys.stderr)
                self.show_usage()
                sys.exit(1)

            _, _, has_param, func, _, _ = self.__opt2info[opt]

            if len(val) == 0 and has_param or len(val) > 0 and not has_param:
                print(f"Incorrect number of arguments for {opt}", file=sys.stderr)
                self.show_usage()
                sys.exit(1)

            if has_param:
                func(val)
            else:
                func()

        if len(args) > 1:
            if self.__out:
                raise RuntimeError("When using multiple templates, -o can not be defined")

        for template in args:
            self._render(template)


if __name__ == "__main__":
    j2rb = J2RenderCLI()

    try:
        j2rb.process_arg(*sys.argv[1:])
    except Exception as e:
        print(f"Error during processing: {e}", file=sys.stderr)
