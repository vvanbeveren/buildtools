# buildtools

Build tools I use in development.

 * version.py  - Provide a version identifier
 * j2render.py - Versatile Jinja2 template renderer with Yaml, JSON5 and CSV input 

Though I do try to keep to the software backwards compatible, there is no guarantee that it will.

## j2render.py help
```
j2render.py [options] <template-file> [<template-file>]

--context=JTEXT,-c JTEXT       Input JSON(5*)-formatted data directly from the command-line
--dir=OUTDIR,-d OUTDIR         Directory to write the output of templates to
--help,-h                      Shows this help
--json=FILE,-j FILE            Input json(5*) data file
--namespace=NS,-n NS           Namespace for the next data
--out=OUTFILE,-o OUTFILE       File to write the output to
--proc=PYFILE,-p PYFILE        Add a context processor (python module)
--table=CSVFILE,-t CSVFILE     Input table data (.csv)
--table-delim=CHAR             Specify table delimiter, specify before next table data***
--table-dialect=DIALECT        Specify table dialect, specify before next table data***
--yaml=FILE,-y FILE            Input yaml(**) data file

  * json5 package must be installed (`pip3 install json5`)
 ** yaml package must be installed (`pip3 install pyyaml`)
*** Unlike namespaces the csv properties are not reset after data input. Thus
    when using these flags to read multiple tables with different delimiters
    or dialects you will need to specify them explicitly before each read.

When using the --dir option, the output filename will be the template name. If
the template ends with .jinja2, .j2, .jinja or .tmpl the extension will be
stripped from the output filename. E.g. 'helloworld.html.j2' will be rendered
into a file named 'helloworld.html'.

Tables are expected in .csv format and the first row must be a header. They
are included row first, as a list of dicts, where each contains the column
header as key and the value for that column at that row. When including a
table the filename, stripped of its last extension, will be used as a
namespace, unless another namespace has been specified first (using --ns). By
default a comma is expected as separator. You can change the separator using
--table-sep option, before issuing the --table option.
```

## version.py help

`version.py [--json] [--out <file>] [git dir]`

