#!/bin/env python3
#
# Script to retrieve the current version based on GIT
#
# History:
# v0.0.1    Vincent van Beveren     Initial version
# v0.0.2    Vincent van Beveren     Added JSON ouput
#
# It creates a version in the following manner:
#
# <git identifier>[-<commit count>]/<git-hash>[<*>]
#
#  * The <git identifier> is the following:
#    - The tag if this commit has a tag
#    - The branch otherwise
#  * The <commit count> is the number of commits since start of the repo
#  * <git hash> is the first 7 digits of the commit hash
#  * <*> is set when the repository is dirty.
#
# The script may be called with a single argument, being the directory inside the repository, otherwise the current
# working directory is used.
#

import subprocess
import sys
from datetime import datetime, timezone

CWD="."

def run_git(*args, none_on_error=True):
    global CWD
    try:
        result = subprocess.run(
            ["git", *args], stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
            check=True, cwd=CWD, encoding='utf-8'
        )
        return result.stdout.strip()
    except subprocess.CalledProcessError:
        if none_on_error:
            return None
        else:
            raise


if __name__ == "__main__":
    json = False
    out_strm = sys.stdout
    args = sys.argv[1:]
    while len(args) > 0:
        if args[0] == "--help":
            print("version.py [--json] [--out <file>] [git dir]")
            sys.exit(0)
        elif args[0] == "--json":
            json = True
        elif args[0] == "--out":
            # so, this is not very nice. But Python is nice
            # and will close the file for us. So thank you python!
            out_strm = open(args[1], "w")
            args = args[2:]
            continue
        elif not args[0].startswith("--"):
            CWD = args[0]
        else:
            print("Unknown argument '%s'" % args[0], file=sys.stderr)
            sys.exit(1)

        args = args[1:]

    git_version = run_git("--version")

    if git_version is None:
        print("Could not run git", file=sys.stderr)
        sys.exit(1)

    git_id = run_git("rev-parse", "--abbrev-ref", "HEAD")
    git_count = int(run_git("rev-list", "--count", "HEAD", none_on_error=False))

    # If there is a tag more recent (or equally) than than the latest branch
    # initialisation, we take the tag, otherwise the branch.
    git_tag = run_git("tag", "--points-at", "HEAD", none_on_error=False)
    if git_tag != "":
        # Handle case in which we have multiple tags.. don't do this, please!
        git_tag = "-".join(git_tag.split("\n"))
        git_id = git_tag

    hash = run_git("rev-parse", "--short", "HEAD", none_on_error=False)
    dirty = run_git("status", "--porcelain", none_on_error=False) != ""

    version = git_id + "-" + str(git_count) + "/" + hash

    if dirty:
        version += "*"

    if json:
        utcnow = datetime.now(timezone.utc).replace(microsecond=0)
        # Json provides an additional date! Woot!
        print('{"version":"%s","date":"%s"}' % (version, utcnow.isoformat()), file=out_strm)
    else:
        print(version, file=out_strm)
